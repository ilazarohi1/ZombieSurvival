# Zombie Survival

Vídeo: https://youtu.be/6yyBC_RNWA4

¡Bienvenidos a Zombie Survival! Este es un emocionante juego de supervivencia en un entorno urbano abierto. El objetivo del juego es sobrevivir en una ciudad infestada de zombies utilizando tus habilidades y recursos disponibles. 

## Características del Juego

- **Entorno Urbano Abierto**: Explora una pequeña ciudad llena de acción y tensión.
- **Personaje Jugable**: Controla a un personaje completamente animado con habilidades para caminar, correr, saltar, girar y disparar.
- **Armas**: Lleva una pistola que dispara en línea recta.
- **HUD**: Siempre visible, muestra el nivel de vida y munición.
- **Enemigos**: Zombies con IA avanzada que patrullan la ciudad y corren hacia ti cuando te detectan.
- **Animaciones de Zombies**: Incluyen caminar, atacar, morir y recibir impactos.
- **Peatones**: Huirán despavoridos si se topan con un zombie.
- **Vehículos**: Coches que circulan autónomamente y que puedes robar para conducir.
- **Efectos Visuales**: Sistemas de partículas cuando te hieren o cuando golpeas o matas a un zombie.
- **Ítems**: Recoge ítems de vida y munición repartidos por la ciudad.
- **Menú de Juego**: Completo y fácil de usar, con opciones para iniciar partida, ajustar volumen y ajustar gráficos.
- **Pantalla de Game Over**: Permite reiniciar el nivel rápidamente.

## Controles

- **Movimiento del Personaje**: `WASD`
- **Saltar**: `Space`
- **Disparar**: `Click Izquierdo del Ratón`

## Requisitos Técnicos

- **Versión de Unity**: 2022.3.20f1
- **Entrada**: Teclado y Ratón